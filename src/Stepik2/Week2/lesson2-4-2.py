# 2.4 Работа с файловой системой и файлами
# Вам дана в архиве (папка main) файловая структура, состоящая из директорий и файлов.
# Вам необходимо распаковать этот архив, и затем найти в данной в файловой структуре все директории,
# в которых есть хотя бы один файл с расширением ".py".
# Ответом на данную задачу будет являться файл со списком таких директорий,
# отсортированных в лексикографическом порядке.
# Для лучшего понимания формата задачи, ознакомьтесь с примером.
# Пример архива(папка sample в каталоге)
# Пример ответа:
# sample
# sample/a
# sample/a/c
# sample/b

import os.path

py_dir = []

if __name__ == '__main__':
    with open("text_py.txt", "w") as w:
        for current_dir, dirs, files in os.walk("main"):
            for i in files:
                if i.endswith(".py"):
                    py_dir.append(current_dir)
                    break

        text = "\n".join(sorted(py_dir))
        w.write(text)

