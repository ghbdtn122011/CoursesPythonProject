# 2.2 Работа с кодом: модули и импорт
# В первой строке дано три числа, соответствующие некоторой дате date -- год, месяц и день.
# Во второй строке дано одно число days -- число дней.
# Вычислите и выведите год, месяц и день даты, которая наступит,
# когда с момента исходной даты date пройдет число дней, равное days.
# Примечание:
# Для решения этой задачи используйте стандартный модуль datetime.
# Вам будут полезны класс datetime.date для хранения даты и класс datetime.timedelta для прибавления дней к дате.
# Sample Input 1:
# 2016 4 20
# 14
# Sample Output 1:
# 2016 5 4
import datetime

date = (int(i) for i in input().split())
days = int(input())

if __name__ == '__main__':
    future_date = datetime.date(*date) + datetime.timedelta(days)
    print(future_date.year, future_date.month, future_date.day)
